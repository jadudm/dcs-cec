
## Future

<div id = "the_future">
</div>

<p> &nbsp; </p>

## Past

<div id = "the_past">

</div>

<style media="screen" type="text/css">

	.upcoming {
	  background-color: #f9efc8;
	}
	
	.urgent {
		background-color: #f4d867;
	}

</style>

<script>
var ndx = 0;
var d = [];

{% for ass in site.assignments %}
    ndx = {{forloop.index0}};
    {% if ass.hour %}
      {% assign hour = ass.hour %}
    {% else %}
      {% assign hour = site.class_start_hour %}
    {% endif %}
    
    {% if ass.minute %}
      {% assign minute = ass.minute %}
    {% else %}
      {% assign minute = site.class_start_minute %}
    {% endif %}
    
    var dobj = getAssignmentMoment("{{site.term_start}}", 
                    {{ass.week}}, 
                    {{ass.day}}, 
                    "{{hour}}", 
                    "{{minute}}");
    dobj.title = "{{ass.title}}";
    dobj.url   = "{{ass.url | prepend: site.baseurl }}.html";
    dobj.type  = "{{ass.type}}";
    //d[ndx] = dobj
    d.push(dobj);
{% endfor %}

function compare(a, b) {
  if (a.the_moment.isBefore(b.the_moment))
    return -1;
  if (b.the_moment.isBefore(a.the_moment))
    return 1;
  return 0;
}

sorted = d.sort(compare);

// Iterate, and build the page.
for (var key in sorted) {
    // check if the property/key is defined in the object itself, not in parent
    if (d.hasOwnProperty(key)) {           
        // Each object is a moment. 
        // If it is before now, put it in the past.
        
				var rowstyle = "";
        var id = "";
        if (d[key].the_moment.isBefore(moment())) {
            id = "#the_past";
        } else {
            id = "#the_future";
						if (d[key].the_moment.diff(moment(), 'days') < 3) {
							rowstyle = "urgent";
						} else if (d[key].the_moment.diff(moment(), 'days') < 5) {
							rowstyle = "upcoming";//"bg-warning";
						}
        }
        
        console.log("Appending to the: " + id + " - " + d[key].title);
				
        var row = $("<div class='row " + rowstyle + "'>");
        var elem = [];
        
        console.log("Week: " + d[key].raw_week + " Day: " + d[key].raw_day + " Hour: " + d[key].hour + " Minute: " + d[key].minute);
        
        var span = $("<span class='badge badge-default'>");
        if (d[key].type == "Reading") {
            span = $("<span class='badge badge-info'>");
        } else if (d[key].type == "Collab") {
            span = $("<span class='badge badge-danger'>");
        } else if (d[key].type == "In Class") {
            span = $("<span class='badge badge-success'>");
        } else if (d[key].type == "Code") {
            span = $("<span class='badge badge-warning'>");
        } else {
          span = $("<span class='badge badge-light'>");
        }
        
        elem.push($("<div class='col-sm-2'>").append(span.append(d[key].type)));
        elem.push($("<div class='col-sm-4'>").append($("<a>").attr("href", d[key].url).html(d[key].title)));
				elem.push($("<div class='col-sm-2'>").html("<small>" + d[key].the_moment.fromNow() + "</small>"));
        elem.push($("<div class='col-sm-4'>").html("<small>" + d[key].display_date + "</small>"));
				
        row.append(elem)
				
				if (d[key].the_moment.isBefore(moment())) {
					$(id).prepend(row);
				} else {
					$(id).append(row);
				}
    }
}
</script>
folders = [ "inclass", "collab", "reading", "writing", "code" ]
typeof =  [ "In Class", "Collab", "Reading", "Writing", "Code" ]
shortcuts = ["ic", "collab", "r", "w", "c"]

import os
def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

def pad(n):
    if n < 10:
        return "0" + str(n)
    else:
        return str(n)

def dow(d):
    days = ["m", "t", "w", "r", "f", "s", "su"]
    return days[d]

for f, sc, to in zip(folders, shortcuts, typeof):
    daynum = 0
    for w in [0, 1, 2, 3, 4]:
        for d in [0, 1, 2, 3]:
            inp  = open("_template.md")
            thefile = pad(w) + "/" + "_" + pad(w) + pad(d) + "-" + dow(d) + "-" + f + ".md"
            if not os.path.isdir(pad(w)):
                os.mkdir(pad(w))
        
            if ((sc == "collab") and (daynum in [1, 3])):
                
                outp = open(thefile, "w")
                for line in inp:
                    print (thefile)
                    line = line.replace("DAYNUM", "Day " + str(daynum + 1))
                    line = line.replace("DAY", str(d))
                    line = line.replace("WEEK", str(w))
                    line = line.replace("CATEGORY", to)
                    outp.write(line)
    
            if (sc in ["ic", "r", "w", "c"]) or ((sc == "collab") and (daynum in [1, 3])):
                outp = open(thefile, "w")
                for line in inp:
                    print (thefile)
                    line = line.replace("DAYNUM", "Day " + str(daynum + 1))
                    line = line.replace("DAY", str(d))
                    line = line.replace("WEEK", str(w))
                    line = line.replace("CATEGORY", to)
                    outp.write(line)
            daynum = daynum + 1

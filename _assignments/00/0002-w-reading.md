---
week:   "0"
day:    "2"
title:  "Making Refuge, To Be Of Use"
type:   "Reading"
slug:   "Understanding where you live."
---

In preparation for our conversation with Sam and Ellen, we're asking you to read selections from [Making Refuge](https://drive.google.com/open?id=11ZrB_6gbpx-CvY6Eg9wiOsCu6DWmR-FB). This will provide some historical context for the socio-political space that Lewiston, Maine inhabits today.

**UPDATED 9:24AM ON WEDNESDAY**: Here is [chapter 4](https://drive.google.com/open?id=1jUQGox8EfG4bid6Cob6rLE4iKgh2sdq1) and [chapter 5](https://drive.google.com/file/d/1LcPhbDslKdO3HpU3UZGWkVIoL0tBrzM4/view?usp=sharing) of the [full book](https://drive.google.com/open?id=1N2a_yQnaNfnqMb72FB0ALkJOuXwuBEnR). We were unaware that the copy we were given was even-numbered pages only.

We will re-post these readings over the next several days so that you have a chance to engagew with them in a reasonable/timely manner.

Piercy's *To Be Of Use* feels appropriate for this day.

## To Be Of Use

Marge Piercy, 1982

The people I love the best<br>
jump into work head first<br>
without dallying in the shallows<br>
and swim off with sure strokes almost out of sight.<br>
They seem to become natives of that element,<br>
the black sleek heads of seals bouncing like half-submerged balls.<br>

I love people who harness themselves, an ox to a heavy cart,<br>
who pull like water buffalo, with massive patience,<br>
who strain in the mud and the muck to move things forward,<br>
who do what has to be done, again and again.<br>

I want to be with people who submerge<br>
in the task, who go into the fields to harvest<br>
and work in a row and pass the bags along,<br>
who are not parlor generals and field deserters <br>
but move in a common rhythm<br>
when the food must come in or the fire be put out.<br>

The work of the world is common as mud.<br> 
Botched, it smears the hands, crumbles to dust. <br>
But the thing worth doing well done<br>
has a shape that satisfies, clean and evident. <br>
Greek amphoras for wine or oil,<br>
Hopi vases that held corn, are put in museums <br>
but you know they were made to be used.<br>
The pitcher cries for water to carry<br>
and a person for work that is real.<br>

## Response

What is the work you will do? Today? Next week? Next year? 

What is the real work you cry out for?

{% include handin.form title="To Be Of Use" %}
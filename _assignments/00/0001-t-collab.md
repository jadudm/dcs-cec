---
week:   "0"
day:    "1"
hour:   "13"
title:  "Design Thinking 101"
type:   "Collab"
slug:   "Designing all the things."
---

During the afternoon, we're going to join forces with our colleagues in Prof. Eames's *Food Pathways* course, and do a deep dive on three different design and collaboration activities.

For reference... 

* The Tower of Soft Doom
* The Wallet of Doom
* The Card Deck of Doom
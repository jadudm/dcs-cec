---
week:   "0"
day:    "1"
title:  "Novice Mind"
type:   "Reading"
slug:   "The Right Answer"
---

## WOTSoTH

Whack on the Side of the Head, or WOTSoTH, is a light read that get you thinking about how you think. 

Read the [intro](https://drive.google.com/open?id=1X99Qyr3O5qPQyBeVb8A1uS8hA3sYJSQi) and the [The Right Answer](https://drive.google.com/open?id=1fDdjZR1XIPjMGl29QZEik2wNEJ6PPnhx), which is the first whack.

## Novice Mind

[Novice Mind](https://drive.google.com/open?id=1RE6AQ0lIn03SSDF0YJ1dDuENuE5ItYK8), by RZA.

I also recommend listening to [Reunited]() from the referenced album.

# Response

What novice mind do you bring to this short term?

{% include handin.form title="Novice Mind" %}
---
week:   "0"
day:    "3"
title:  "Community Partners"
type:   "Collab"
slug:   "Meet with your partners."
---

More detail here, and we'll talk about this in class. 

Generally speaking, Thursdays will always be time for you to work as a group and meet with your community partners. 

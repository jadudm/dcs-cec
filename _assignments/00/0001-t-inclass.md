---
week:   "0"
day:    "1"
title:  "Creativity and Collaboration"
type:   "In Class"
slug:   "Diving back into code."
---

We'll begin with some conversation about creativity and collaboration. A goal of that conversation will be to develop a grading contract for our team collaborations this short term.

After that conversation, we'll move on to implementing Paint Pot, which will introduce variables.
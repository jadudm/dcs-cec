---
week:   "0"
day:    "3"
title:  "Community and Code"
type:   "In Class"
slug:   "Tools for collaboration."
---

We'll spend the first hour doing some programming. Time is flying! We'll start with Mole Mash.

We will spend the 11AM - noon hour working developing some comfort with tools for electronic collaboration and communications. We'll introduce you to Slack, Trello, and make sure you're set up on Google Apps for effective collaboration in your groups.

## Afternoon

We'll begin with some writing to think and conversation around our session with the Harward Center team and the readings from Wednesday. This will be a chance to make sure you're ready for 

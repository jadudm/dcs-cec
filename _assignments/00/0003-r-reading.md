---
week:   "0"
day:    "3"
title:  "Reading in PoCE"
type:   "Reading"
slug:   "That's Not Logical"
---

Following our conversations with our colleagues in the Harward Center, we will want to do a bit more reading and reflection on the nature of community engagements.

## App Inventor

For Thursday, you will want to skim chapters [1](http://www.appinventor.org/bookChapters/chapter1.pdf) and [2](http://www.appinventor.org/bookChapters/chapter2.pdf) of the App Inventor book. We'll be doing both of these in class.

In the afternoon, we'll spend a bit of time getting ready for our meeting with community partners, and then... get together with our community partners!

{% comment %}
1. [Characteristics of Culturally Competent Organizations](http://www.health.state.mn.us/communityeng/multicultural/character.html)<br>
    You will want to be cognizant of who you are, who you are working with, and how you are working with your partners. Are you coming from a place of collaboration and mututal respect, or somewhere else?
2. [Ground Rules for Cross-Cultural Dialog](http://www.health.state.mn.us/communityeng/multicultural/cross.html)<br>
    How do you engage in substantive conversations with collaborators whose lived experiences are very different from your own?
3.[Chapter 2: Principles of Community Engagement](https://drive.google.com/open?id=1pg0rQiKhxIwUSXX7g0Qnzw9pAX9C2keP)
4.[Chapter 5: Challenges in Improving Community Engaged Research](https://drive.google.com/open?id=18nmngHmawL76ItFW9QDn_xcGaINEIUk1)

The last chapter, at 41 pages, gets a bit long. However, you should read the section headings, and only read headings that you think apply to your work. For example, you do *not* need to read about double-blind drug testing in a community-engaged context! That said, questions of historical exploitation, or maintaining connection to your partners are things you might want to read. 

In other words: there's a lot of good in there, but you have to do some work to filter, translate, and extract.
{% endcomment %}


## Collaborate

[Collaborate](https://vimeo.com/channels/600445/71372936).

## WOTSoTH

[That's Not Logical.](https://drive.google.com/open?id=1ijsgsM91rzxF4WL1y54zaqj7mTmT8XQw).

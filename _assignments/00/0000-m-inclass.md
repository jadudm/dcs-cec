---
week:   "0"
day:    "0"
title:  "Introduction"
type:   "In Class"
slug:   "Day one."
---

Today we will:

* Introduce ourselves and each-other.
* Lay out the course, our community partners, our goals, and how we will assess our efforts.
* We'll dive into the hands-on work of learning some programming.

We'll be using a chapter from the [App Inventor Book](http://www.appinventor.org/bookChapters/chapter1.pdf) to guide our work in class. This will often be the case.
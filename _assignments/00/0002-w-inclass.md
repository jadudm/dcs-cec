---
week:   "0"
day:    "2"
title:  "The Ellen and Sam Show"
type:   "In Class"
slug:   "All the way from the Harward Center in Lewiston, ME!"
---

We'll be spending class with the Harward Center team. The readings are essential preparation for this conversation.

## Project Choices

[A quick form](https://goo.gl/forms/SMYSlBXJanzuiR6x1) that you can complete at the start of class. Let us know what your project preferences would be. 
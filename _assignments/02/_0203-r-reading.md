---
week:   "2"
day:    "3"
title:  "Day 12"
type:   "Reading"
slug:   "E Puribus Unum"
---

## Diversity and Community in the 21st Century

[E Puribus Unum](https://drive.google.com/open?id=1oi2zya2vdUTWQBJvW83itGdJNOmMyu7u)

{% include handin.form title="Diversity and Community" %}
---
week:   "2"
day:    "0"
title:  "Update Presentations"
type:   "In Class"
slug:   ""
---

We'll begin Monday by taking 8 minutes to put together some slides and approximately 10 minutes each (4m to present, 6m for discussion) to update everyone on the status of your work.

Your slides should all be title slides, with just one word on them. This will force you to think about the talk as an outline, not as a set of slides with a lot of words on them. Imagine having anywhere from 4 slides (1m each) to 8 slides (30s each). It is reasonable for multiple people to present, but do not put everything on one person.

This is an exercise in concision; you know the project well. Providing an update may also help clarify where you're at for yourselves.

What we might do, if it makes sense, is for you to break into some mixed groups and discuss what the biggest questions/sticking points are, and take a few minutes to brainstorm and discuss with "fresh eyes" on those challenges.

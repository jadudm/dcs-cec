---
week:   "2"
day:    "2"
title:  "Databases, Read What You Need"
type:   "Reading"
slug:   "That's Not my Area"
---

## Building Databases

[Storing a lot of data requires a database.](https://drive.google.com/open?id=1Lv-yGTL8pS6i_1DO8JNFjHNopxo-V2nd) And, you'd be surprised how often what seems like a "small" amount of data would benefit from a database.

## Reading for your Project

What reading do you need to be doing for your project? Do it. 

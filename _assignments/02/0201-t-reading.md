---
week:   "2"
day:    "1"
title:  "Write Like a Motherfucker"
type:   "Reading"
slug:   "It's the title of an article."
---

## Reusing Code

[Functions are a critical abstraction in programming](https://drive.google.com/open?id=12A8ex1EGpaX3VpIWtFxh_uh9h6TdlnGb). They let us give a name to a set of actions or operations, and use those over-and-over, sometimes with parameters that let us change the behavior of that function. 

## Stayed

[Write Like a Motherfucker](https://drive.google.com/open?id=1IEnpxIPagIoHF2EaqehFksoijbUR_Ja2)

## Response

What words are on opposite sides of your chalkboard?

Why?

And in what way are you going to go out into the world and ___________ like a motherfucker?

{% include handin.form title="Blank Like a Motherfucker" %}
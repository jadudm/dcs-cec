---
week:   "2"
day:    "1"
title:  "Debrief, Work"
type:   "In Class"
slug:   "Update and Go"
---

At this point, we'll shift to a brief, daily update. 

* What did you do yesterday?
* What are you working on today?
* Are there any roadblocks?

Every morning, we'll ask this question of each group.

## Tuesday PM

Work time. We're available for consultation.

This is where we are in the projects:

<a href='https://photos.google.com/share/AF1QipMZK-vb6PC198hltoneYOoWEExV-gnTTr7i8P-HaHMT_SOK-ZmHlkqjaAhMpVHumg?key=RUdZY1JzdUpldzM3QkFwYWYxQTV4aTFjQld3blpR'><img src='https://lh3.googleusercontent.com/qoy32g3mpJ1u_VC8ghyFeTIU94vmPZCA6RuISOHo1mA_b6rpbVsE8P9caM0GM65pSYg-nMKMGQ68ZFsdwXiPmJ1cyqj-zf-dBXaR1Ox5OQTYycCXsQU8-Oi9rgyzSxczkPBjAZyX2g=w2400' /></a>

You've drawn some circles. Now, *draw the owl*.

Roughly.
---
week:   "2"
day:    "0"
title:  "Independent Coding"
type:   "Code"
slug:   ""
---

You should have identified what the next steps in learning are for your group.

For example, you may have decided that a chapter on using GPS was important, or databases. You should have worked two chapters this weekend to move yourself forward in your App Inventor understanding. 

If you're working with another environment (eg. Scratch), you might do the same, but different.

Point being: keep moving on your *practical* or *applied* understanding of programming in App Inventor. **Come in to class with questions, or ask them on Slack as you are working**.

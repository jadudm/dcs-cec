---
week:   "2"
day:    "0"
title:  "Conditionals, Lists, Loops"
type:   "Reading"
slug:   "Play is Frivolous"
---

## Conditionals, Lists, and Loops... Oh My!

**These are foundational concepts that all groups will benefit from reading about**.

How do we make decisions in our programming? [Conditional statements](https://drive.google.com/open?id=1JuMfCzp-I1X93JdpTleait9GPfrMDsst) (sometimes called "if" statements).

[Lists](https://drive.google.com/open?id=1B424WmFsd-BiobCjVfTBej3FcplI5XA4) are just what they sound like. Eggs, milk, butter... a list of things you need when going shopping. Or a list of student names. Or mural tiles. Or game pieces. Or a game board.

You should also know how to do something with every item in a list. These are called [loops](https://drive.google.com/open?id=1GM8sqoTAvNoKV584L_B4gWGn9fD_tIgm).

## WOTSoTH

[Play is Frivolous](https://drive.google.com/open?id=1CzgDtAhsmAWW5lRVxudV40bifA-wVdxd)

## Response

What role has play played in your project so far?

Has it played enough of a role? Should it play more of a role?

What problems might it help you move past if you took a more playful approach to what you're working on?

{% include handin.form title="Time To Work and Time To Play" %}
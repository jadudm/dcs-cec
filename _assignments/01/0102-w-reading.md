---
week:   "1"
day:    "2"
title:  "Manifesto for Growth"
type:   "Reading"
slug:   "Avoid Ambiguity"
---

## Manifesto

[Manifesto for Growth](https://drive.google.com/open?id=1p76c5_GP4h62eYmM1zR4beYh_Y198QPb). Mau.

## Make Others Successful

[Make Others Successful](https://vimeo.com/channels/600445/71932776)

## WOTSoTH

[Avoid Ambiguity](https://drive.google.com/open?id=1Bxw3z9wQQ1uvZNvINgCAp9Ro3wRTiiND).

## Response

Pick four songs as a soundtrack. 

Queue them up.

Put them on repeat.

Write your manifesto.

*I have a friend who listened to the 60 minute club remix of "What Is Love." He showed me his iPod at one point. The repeat count was on 17. Do not let this happen to you.*

{% include handin.form title="My Manifesto" %}
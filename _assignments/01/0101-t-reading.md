---
week:   "1"
day:    "1"
title:  "The Rules of Improvisation"
type:   "Reading"
slug:   "Follow the Rules"
---

## App Architecture

You've begun exploring App Inventor, but that isn't the same as understanding the conceptual pieces of an app. [Chapter 14](https://drive.google.com/open?id=1T0quCm7f2U4umeHRxQjZrY_50BYmJifh) explores that.

## Rules of Improvisation

The [Rules of Improvisation](https://drive.google.com/open?id=1tFuerTj_mP7102yoL3lcKr9CgDvaxIn2) by Fey.

## WOTSoTH

[Follow the Rules](https://drive.google.com/open?id=1JUt_qQUhY_vigGOwxs7HP-w5Fa75rmGa).

## Response

Can you plan fastidiously and still leave room for improvisation?

What role has improvisation played in your education?

What role could it play?

What are the rules that you will improvise by after you graduate?

{% include handin.form title="Rules of Improvosation" %}
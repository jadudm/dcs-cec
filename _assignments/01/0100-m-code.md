---
week:   "1"
day:    "0"
title:  "Mole Mash"
type:   "Code"
slug:   "Bang bang! Went Maxwell's silver hammer..."
---

Over the weekend, grab Chapter 3 of the App Inventor book ([here's all of the chapters](https://drive.google.com/open?id=1lOMbw8O8_mIejqBTJcw6rRj-c2StxYAQ), or [from the App Inventor site](http://www.appinventor.org/book2)), and work through Mole Mash. 

We'll talk about some of the concepts you're learning on Monday, and make sure you see how they relate to your various project groups. Although the context is a silly little game (unless we do not think Mole Mash is silly... it could be *very, very serious*), there's a lot of core concepts that we're seeing that I want to make sure are highlighted.

We'll start with questions on Monday, do some highlights, and move on to Chapter 4.

{% include handin.form title="Mole Mash" %}

Download your APK and upload it here so we can take a look at your Mole Mash awesomeness.
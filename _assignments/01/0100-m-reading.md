---
week:   "1"
day:    "0"
title:  "Making Refuge, Rocket Surgery"
type:   "Reading"
slug:   "Be Practical"
---

The challenge of short term is packing learning into a short period of time. We're trying to curate the things you might try and skim/learn/absorb as much as possible, but it will still take time and effort on your part. We can't change that. We use in-class time, as much as possible, for collaborative kinds of learning, or introducing things that are new, and out-of-cass time for things that we are confident you can engage with on-your-own.

## Making Refuge

Here is the full [chapter 4](https://drive.google.com/open?id=1jUQGox8EfG4bid6Cob6rLE4iKgh2sdq1) from *Making Refuge*. You may have already gleaned enough from the... every-other-page version that we posted last week. 

If you want the full book, you can get it through the library, or [you can download it here](https://drive.google.com/open?id=1N2a_yQnaNfnqMb72FB0ALkJOuXwuBEnR).

## Rocket Surgery Made Easy

I would recommend [skimming the first six chapters of RSME](https://drive.google.com/open?id=1MKefZ8c9ZJo-_kJJbABkFbpT4F--obHW). This may be particularly useful to the Gleaning and Mural groups.

## Game Design and Development

Hamish has shared a number of readings with the game dev group. We'll transfer those here in time, but for now, check your email.

## Continued Readings on Creativity

Finally, we have an ongoing series of readings about design and creativity (and the dangers of thinking yourself uncreative). 

[Teach Yourself Italian](https://drive.google.com/open?id=1V0TMKHVN-HhHtgdlmO-W9USEV0yyh9Bj) by Lahiri.

[Be Practical](https://drive.google.com/open?id=1c09WzDJXzRY0kdApBjxI016P_0H6rBp8)

## Response

Write your [creative autobiography](https://drive.google.com/open?id=1TgJVAIqZAuLZ-faeZpvGbXNqE0zCOxn3). You may blog it if you wish, or you may keep it private. (The submission link below only goes to the faculty.)

Please bring a printed copy to class.

{% include handin.form title="Creative Autobiography" %}
---
week:   "1"
day:    "3"
title:  "Hard Fun, Alientologists"
type:   "Reading"
slug:   "To Err Is Wrong"
---

## App Memory

How do things get stored in an app? [Chapter 16](https://drive.google.com/open?id=16iYWQTBuokEHmJbEgqNxh_bnu9kuAtxv) talks about variables and the storage of data.

## Papert

[Hard Fun](https://drive.google.com/open?id=1sdoTbx-cdaBaEyFVP5GlY02HiPouEUPg).

## To Err Is Wrong

[To Err Is Wrong](https://drive.google.com/open?id=1d_P0dhqY-Amd1a9sgxwMfvL9AOgsCrtL)

## Alientologists

Just watch it straight through. With sound. Don't skip around. Don't skip forward. Just enjoy it. 15 minutes.

<iframe width="560" height="315" src="https://www.youtube.com/embed/OmCCJHnYOUU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Response

What about you might seem alien to another?

What do you have a hard time relating to?

How might that be a liability in your work with your community collaborators? 

How could it be a benefit?

*You might chose for this to be a private reflection.*

{% include handin.form title="Relating to Difference" %}
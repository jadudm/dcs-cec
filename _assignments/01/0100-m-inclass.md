---
week:   "1"
day:    "0"
title:  "Programming Concepts, Tech for Collab"
type:   "In Class"
slug:   ""
---

We'll begin with some writing to think about the readings. 

We'll continue with an overview of programming concepts you've seen so far, and then move on to technologies that might be useful to you as you collaborate.




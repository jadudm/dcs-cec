---
week:   "1"
day:    "1"
title:  "All Planning"
type:   "In Class"
slug:   ""
---

In the morning, we will be missing one group as they meet with their community partners. The remaining groups (the Middle School groups) may be able to meet with their partners. We'll spend that time working with Matt and Hamish if not.

In the afternoon, we have planned a Planning Extravaganza. We'll be using some old-fashioned paper tools to introduce you to planning.


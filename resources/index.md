---
title: Resources
layout: default
---

# Resources

Here's a collection of resources that either we've assigned for reading or think might generally be useful.

# Class Readings

* [Making Refuge](https://drive.google.com/open?id=1N2a_yQnaNfnqMb72FB0ALkJOuXwuBEnR)
* [App Inventor](https://drive.google.com/open?id=1lOMbw8O8_mIejqBTJcw6rRj-c2StxYAQ)





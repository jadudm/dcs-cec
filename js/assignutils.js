
function getAssignmentMoment(term_start, week, day, start_hour, start_minute) {
    var dmom            = moment(term_start);
    dmom.add(week, 'weeks').add(day, 'days');
    dobj = {};
    dobj.raw_week       = week;
    dobj.raw_day        = day;
    dobj.year           = dmom.format("YYYY");
    dobj.month          = dmom.format("MM");
    dobj.day            = dmom.format("DD");
    dobj.hour           = start_hour;
    dobj.minute         = start_minute;
    var hour_padded     = "";
    if (start_hour < 10) {
      hour_padded = "0" + start_hour;
      } else { hour_padded = "" + start_hour;
    }
    var time_to_parse       = term_start.replace(/-/g, "") + "T" + hour_padded + "" + start_minute;
    
    var the_moment    = (moment(time_to_parse)
      .add(week, 'weeks')
      .add(day, 'days'));
    dobj.the_moment = the_moment;
    
    var display_time = the_moment.format("dddd, MMM Do HH:mm");
    // console.log("Parse: " + time_to_parse);  
    // console.log("Parsed: " + display_time);
    dobj.display_date   = display_time;
    
    return dobj;
}

function submissionLink(title, term_start, week, day, start_hour, start_minute) {
    return $("<a><i class='fas fa-cloud-upload-alt fa-1x'></i></a>").attr("href", ("https://docs.google.com/forms/d/e/1FAIpQLSfP3XzK6WDypkNVw8X_NiuZYFg3fOrooPfoaxhHGZt37AvNRA/viewform?usp=pp_url&"
      + "entry.37685363=" 
      + title
      + "&entry.1208641331="
      + (getAssignmentMoment(term_start, week, day, start_hour, start_minute)
        .the_moment
        .format("YYYY-MM-DD"))
      ));
} 
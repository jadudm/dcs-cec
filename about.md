---
layout: page
title: About
permalink: /about/
---

This is the Winter 2018 offering of DCS102, and it is likely to go down in history as a unique course offering.

### Instructors

[Matt Jadud](mailto:mjadud@bates.edu)

[Hamish Cameron](mailto:hcameron@bates.edu)